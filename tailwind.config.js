module.exports = {
  plugins: [require('@tailwindcss/forms')],
  content: ['src/**/*.{clj,cljs}'],
  theme: {
    extend: {
      fontFamily: {
        sans: ['Titillium Web', 'sans-serif'],
        serif: ['Playfair Display', 'serif'],
        //mono: ['Space Mono', 'SpaceMono-Regular', 'monospace'],
      },


      colors: {
        'bootstrap-blue': '#0d6efd',

        // style guide color
        'dark-navy':        '#002838',
        'cool-gray':        '#FBF9F6',
        'steel-blue':       '#629AA6',
        'mint-green':       '#8DFFBC',
        'brass':            '#D7AB51',
        'peach':            '#FEC196',
        'dark-tan':         '#C8B79B',
        'light-steel-blue': '#DFEBED',
        'snow-cloud':       '#E5E9EB',
      },
    },
  },
};

