(ns app.ui.demo.intro
  (:require
   [com.fulcrologic.fulcro.components :as comp :refer [defsc]]
   [com.fulcrologic.fulcro.dom :as dom]))

(defsc Intro [this {:keys [] :as props}]
  {:query         []
   :ident         (fn [] [:component/id :intro])
   :route-segment ["intro"]
   :initial-state {}}
  (dom/div :.p-4.space-y-16.max-w-3xl
    (dom/div :.text-5xl.font-serif "Intro")

    (dom/div :.space-y-4
      (dom/div :.text-3xl "Abstract")
      (dom/p :.pb-4.text-justify
        "Tailwind is a utility-first CSS framework that is very popular in "
        "JavaScript development, but is it a fit for ClojureScript development? "
        "In this talk, Norman will introduce Tailwind, demonstrate its usage in "
        "a ClojureScript and give his opinions on what kinds of ClojureScript "
        "projects and teams are a good match for Tailwind."))


    (dom/div :.space-y-4
      (dom/div :.text-3xl "A common experience ")
      (dom/section :.text-center.px-8.border-l-2
                   #_(dom/h2 :.text-slate-900.text-4xl.tracking-tight.font-extrabold.sm:text-5xl.dark:text-white
                             "“Best practices” don’t actually work.")
                   (dom/figure
                     (dom/blockquote
                       (dom/p
                         :.mt-6.max-w-3xl.mx-auto.text-lg
                         "I’ve written "
                         (dom/a
                           :.text-sky-500.font-semibold.dark:text-sky-400
                           {:href
                            "https://adamwathan.me/css-utility-classes-and-separation-of-concerns/"}
                           " a few thousand words ")
                         "on why traditional “semantic class names” are the reason CSS is hard to maintain, but the truth is you’re never going to believe me until you actually try it. If you can suppress the urge to retch long enough to give it a chance, I really think you’ll wonder how you ever worked with CSS any other way."))
                     (dom/figcaption
                       :.mt-6.flex.items-center.justify-center.space-x-4.text-left
                       (dom/img
                         :.w-14.h-14.rounded-full
                         {:src "https://tailwindcss.com/_next/static/media/adam.26d0119c.jpg",
                          :alt "",
                          :loading "lazy",
                          :decoding "async"})
                       (dom/div
                         (dom/div
                           :.text-slate-900.font-semibold.dark:text-white
                           "Adam Wathan")
                         (dom/div :.mt-0.5.text-sm.leading-6 "Creator of Tailwind CSS")))))

      (dom/div
        "I laugh at this quote because I often told people that when I started
my current project and saw Tailwind, I wanted to throw up. It made by
eyes bleed."))


    (dom/div :.space-y-4.snap-y
      (dom/div :.text-3xl "My first exposure to tailwind was something like this")
      (dom/img {:src "/code.png"})

      (dom/div
        "It's a lot to take in, though it's not all Tailwind's fault. I tried to "
        "undo this mess, but everything I tried was painful and every time I tried "
        "the tailwind way, things just worked."))


    (dom/div :.space-y-4
      (dom/div :.text-3xl "What is tailwind anyway?")

      (dom/ul :.list-disc.ml-8
        (dom/li "a \"utility-first\" CSS framework")
        (dom/li "“an API for your design system”")))





    (dom/div :.space-y-4
      (dom/div :.text-3xl "Utility first - a classic comparison")
      (dom/ul :.list-disc.ml-8
        (dom/li "We've probably all used bootstrap. It was (is?) awesome")
        (dom/li "Bootstrap is a CSS library that implements a specfic design system")
        (dom/li "Current bootstrap has has a utility API similar to tailing in some ways, but let's think back to old school bootstrap")

        (dom/li "What do you remember? What I remember is that you could always recognize a bootstrap site just by looking at it")
        (dom/li "You could theme it, you could extend components, you could create your own components, but it was always bootstrap")
        (dom/li "Bootstrap has lots of components and it has opinions on how to use them")
        (dom/li "Tailwind is sort of the opposite - with tailwind you build up your own design system")))


    (dom/div :.space-y-4
      (dom/div :.text-3xl
        (dom/a {:href "https://tailwindcss.com/#constraint-based"}
          "An API for your design system"))

      (dom/div :.border-l-2.px-2
        "Utility classes help you work within the constraints of a
        system instead of littering your stylesheets with arbitrary
        values. They make it easy to be consistent with color choices,
        spacing, typography, shadows, and everything else that makes
        up a well-engineered design system."))



    ))






