(ns app.ui.demo.card-demo
  (:require
   [com.fulcrologic.fulcro.components :as comp :refer [defsc]]
   [com.fulcrologic.fulcro.dom :as dom]))

(defsc Card [this props]
  {}
  (dom/div :.border.bg-white.rounded-md.overflow-hidden
    (merge-with str props
                {:className " border-black/20"})

    (comp/children this)))

(def ui-card (comp/factory Card))

(defsc CardBody [this props]
  {}
  (dom/div :.p-4
    props
    (comp/children this)))

(def ui-card-body (comp/factory CardBody))

(defsc CardTitle [this props]
  {}
  (dom/div :.text-xl.font-bold.mb-2
    props
    (comp/children this)))

(def ui-card-title (comp/factory CardTitle))

(defsc CardText [this props]
  {}
  (dom/p :.mb-4 props (comp/children this)))

(def ui-card-text (comp/factory CardText))

(defsc Button [this props]
  {}
  (dom/a :.text-white.px-3.py-1.rounded-md
    (merge-with str
                props
                {:className " bg-[#0d6efd] hover:bg-[#0B5ED7]"})
    (comp/children this)))

(def ui-button (comp/factory Button))

(defsc ImageTop [this props]
  {}
  (dom/img :.object-cover props))
(def ui-image-top )


(defsc CardDemo [this {:keys [] :as props}]
  {:query         []
   :ident         (fn [] [:component/id :card-demo])
   :route-segment ["card-demo"]
   :initial-state {}}
  (dom/div :.p-4.space-y-4
    (dom/div :.text-5xl.font-serif "Card Example")

    (dom/div :.pb-4
      "This page demonstrates the use of utility classes using the classic "
      (dom/a :.hover:underline {:href "https://getbootstrap.com/docs/5.3/components/card/"}
        "Bootstrap card") " component as an example.")

    (dom/div :.flex.flex-row.space-x-8

      ;; bootstrap style
      (dom/div :.card.w-72
        (dom/img :.card-img-top {:src "https://picsum.photos/300/200?a"})
        (dom/div :.card-body
          (dom/h5 :.card-title "Bootstrap Card")
          (dom/p :.card-text
            "Some quick example text to build on the card title and make up the bulk of the card's content.")
          (dom/a :.btn.btn-primary {:href "#"} "Go somewhere")))

      ;; direct code
      (dom/div :.w-72.border.bg-white.rounded-md.overflow-hidden.shadow-sm.hover:shadow-lg
        {:className "border-black/20"}

        (dom/img :.object-cover {:src "https://picsum.photos/300/200?b"})

        (dom/div :.p-4
          (dom/div :.text-xl.font-bold.mb-2 "Inline Tailwind")
          (dom/p :.mb-4
            "Some quick example text to build on the card title and make up the bulk of the card's content.")

          (dom/a :.text-white.px-3.py-1.rounded-md.self-start.justify-self-end
            {:className "bg-[#0d6efd] hover:bg-[#0B5ED7]"
             :href "#"}
            "Go somewhere")))

      ;; component
      (ui-card
        {:className "w-72 shadow-sm hover:shadow-lg"}
        (dom/img :.object-cover {:src "https://picsum.photos/300/200?c"})
        (ui-card-body {}
          (ui-card-title {}
            "Component Card")
          (ui-card-text {}
            "Some quick example text to build on the card title and make up the bulk of the card's content.")
          (ui-button {:href "#"}
            "Go somewhere"))))))





