(ns app.ui.root
  (:require
    [app.model.session :as session]
    [app.ui.demo.card-demo :refer [CardDemo]]
    [app.ui.demo.intro :refer [Intro]]
    [clojure.string :as str]
    [com.fulcrologic.fulcro.dom :as dom ]
    [com.fulcrologic.fulcro.dom.html-entities :as ent]
    [com.fulcrologic.fulcro.dom.events :as evt]
    [com.fulcrologic.fulcro.application :as app]
    [com.fulcrologic.fulcro.components :as comp :refer [defsc]]
    [com.fulcrologic.fulcro.routing.dynamic-routing :as dr]
    [com.fulcrologic.fulcro.ui-state-machines :as uism :refer [defstatemachine]]
    [com.fulcrologic.fulcro.mutations :as m :refer [defmutation]]
    [com.fulcrologic.fulcro.algorithms.merge :as merge]
    [com.fulcrologic.fulcro.algorithms.form-state :as fs]


    [taoensso.timbre :as log]))

(defn field [{:keys [label valid? error-message] :as props}]
  (let [input-props (-> props (assoc :name label) (dissoc :label :valid? :error-message))]
    (dom/div
      (dom/label {:htmlFor label} label)
      (dom/input :.px-2.py-1.rounded-md input-props)
      (dom/div :.text-red-300.text-sm {:classes [(when valid? "hidden")]}
        error-message))))

(defsc SignupSuccess [this props]
  {:query         ['*]
   :initial-state {}
   :ident         (fn [] [:component/id :signup-success])
   :route-segment ["signup-success"]}
  (dom/div
    (dom/h3 "Signup Complete!")
    (dom/p "You can now log in!")))

(defsc Signup [this {:account/keys [email password password-again] :as props}]
  {:query             [:account/email :account/password :account/password-again fs/form-config-join]
   :initial-state     (fn [_]
                        (fs/add-form-config Signup
                                            {:account/email          ""
                                             :account/password       ""
                                             :account/password-again ""}))
   :form-fields       #{:account/email :account/password :account/password-again}
   :ident             (fn [] session/signup-ident)
   :route-segment     ["signup"]
   :componentDidMount (fn [this]
                        (comp/transact! this [(session/clear-signup-form)]))}
  (let [submit!  (fn [evt]
                   (when (or (identical? true evt) (evt/enter-key? evt))
                     (comp/transact! this [(session/signup! {:email email :password password})])
                     (log/info "Sign up")))
        checked? (fs/checked? props)]
    (dom/div
      (dom/h3 "Signup")
      (dom/div {:className (when checked? "error")}
        (field {:label         "Email"
                :value         (or email "")
                :valid?        (session/valid-email? email)
                :error-message "Must be an email address"
                :autoComplete  "off"
                :onKeyDown     submit!
                :onChange      #(m/set-string! this :account/email :event %)})
        (field {:label         "Password"
                :type          "password"
                :value         (or password "")
                :valid?        (session/valid-password? password)
                :error-message "Password must be at least 8 characters."
                :onKeyDown     submit!
                :autoComplete  "off"
                :onChange      #(m/set-string! this :account/password :event %)})
        (field {:label         "Repeat Password" :type "password" :value (or password-again "")
                :autoComplete  "off"
                :valid?        (= password password-again)
                :error-message "Passwords do not match."
                :onChange      #(m/set-string! this :account/password-again :event %)})
        (dom/button :.ui.primary.button {:onClick #(submit! true)}
                    "Sign Up")))))

(declare Session)

(defsc Login [this {:account/keys [email]
                    :ui/keys      [error open?] :as props}]
  {:query         [:ui/open? :ui/error :account/email
                   {[:component/id :session] (comp/get-query Session)}
                   [::uism/asm-id ::session/session]]
   :initial-state {:account/email "" :ui/error ""}
   :ident         (fn [] [:component/id :login])}
  (let [current-state (uism/get-active-state this ::session/session)
        {current-user :account/name} (get props [:component/id :session])
        initial?      (= :initial current-state)
        loading?      (= :state/checking-session current-state)
        logged-in?    (= :state/logged-in current-state)
        password      (or (comp/get-state this :password) "")] ; c.l. state for security
    (when-not initial?
      (if logged-in?
        (dom/button
            {:onClick #(uism/trigger! this ::session/session :event/logout)}
            (dom/span current-user) ent/nbsp "Log out")
        (dom/div :.relative
          {:onClick #(uism/trigger! this ::session/session :event/toggle-modal)}
          "Login"
          (when open?
            (dom/div :.absolute.top-0.right-10.border.bg-white.p-4.rounded-md.shadow-md
              {:onClick (fn [e]
                          ;; Stop bubbling (would trigger the menu toggle)
                          (evt/stop-propagation! e))}
              (dom/h3 :.text-2xl "Login")
              (dom/div {:classes [(when (seq error) "error")]}
                (field {:label    "Email"
                        :value    email
                        :onChange #(m/set-string! this :account/email :event %)})
                (field {:label    "Password"
                        :type     "password"
                        :value    password
                        :onChange #(comp/set-state! this {:password (evt/target-value %)})})

                (dom/div :.my-4
                  (dom/button :.border.rounded-md.px-4.py-2.text-md
                    {:onClick (fn [] (uism/trigger! this ::session/session :event/login {:username email
                                                                                         :password password}))
                     :classes [(when loading? "loading")]} "Login"))
                (dom/div
                  (dom/p "Don't have an account?")
                  (dom/a {:onClick (fn []
                                     (uism/trigger! this ::session/session :event/toggle-modal {})
                                     (dr/change-route this ["signup"]))}
                    "Please sign up!"))))))))))

(def ui-login (comp/factory Login))

(defsc Main [this props]
  {:query         [:main/welcome-message]
   :initial-state {:main/welcome-message "Hi!"}
   :ident         (fn [] [:component/id :main])
   :route-segment ["main"]}
  (dom/div :.p-4.space-y-4
    (dom/div :.text-5xl.mb-8.font-serif "Tailwind Demo App")
    (dom/p :.max-w-3xl
      "This is a demo app based on the "
      (dom/a {:href "https://github.com/fulcrologic/fulcro-template"} "fulcro template")
      ", Demonstrating how to use "
      "tailwind with ClojureScript application. The styles are loosely based on the "
      "Skipp style guide.")


    (dom/div :.text-xl.font-bold "Adding Tailwind")
    (dom/ul :.list-disc.ml-8
      (dom/li :.font-mono "npx yarn add -DE tailwindcss@latest")
      (dom/li :.font-mono "npx yarn add -DE @tailwindcss/forms@latest")
      (dom/li :.font-mono "npx tailwindcss init # creates tailwind.config.js")
      (dom/li "added \""
              (dom/span :.font-mono "resources/public")
              "\" to shadow-cljs watch-dir")
      (dom/li "added '"
              (dom/span :.font-mono "src/**/*.{clj,cljs}")
              "' to tailwind.config.js content dir"))


    (dom/div :.text-xl.font-bold "How I am running this ")
    (dom/ul :.list-disc.ml-8
      (dom/li :.font-mono "npx yarn install ; install JS deps")
      (dom/li :.font-mono
              "alias shadow='/usr/bin/npx shadow-cljs -d nrepl/nrepl:1.0.0 -d cider/cider-nrepl:0.30.0 -d refactor-nrepl/refactor-nrepl:3.6.0 -d cider/piggieback:0.5.2'")

      (dom/li :.font-mono  (dom/span :.font-bold "shadow watch main ")
              (dom/span :.font-light "; I use a global shadow alias across projects that keeps nrepl deps across projects"))
      (dom/li :.font-mono "npx tailwindcss --output resources/public/css/app.css --watch ; the important part for tailwing")

      (dom/li "C-U C-C M-j adding :dev mode")
      (dom/li "in development ns, (start)"))))

(defsc Settings [this {:keys [:account/time-zone :account/real-name] :as props}]
  {:query         [:account/time-zone :account/real-name :account/crap]
   :ident         (fn [] [:component/id :settings])
   :route-segment ["settings"]
   :initial-state {}}
  (dom/div
    (dom/h3 :.text-2xl "Settings")
    (dom/div "TODO")
    (dom/form :.flex.flex-col.space-y-8.mt-8.border
      (dom/div
        (dom/input {:type "text" :placeholder "text goes here"}))
      (dom/div
        (dom/input {:type "text" :placeholder "text goes here"})))))

(dr/defrouter TopRouter [this props]
  {:router-targets [Main
                    ;; Signup
                    ;; SignupSuccess
                    ;; Settings

                    ;; new routes for the demo
                    CardDemo
                    Intro]})

(def ui-top-router (comp/factory TopRouter))

(defsc Session
  "Session representation. Used primarily for server queries. On-screen representation happens in Login component."
  [this {:keys [:session/valid? :account/name] :as props}]
  {:query         [:session/valid? :account/name]
   :ident         (fn [] [:component/id :session])
   :pre-merge     (fn [{:keys [data-tree]}]
                    (merge {:session/valid? false :account/name ""}
                      data-tree))
   :initial-state {:session/valid? false :account/name ""}})

(def ui-session (comp/factory Session))

(defsc TopChrome [this {:root/keys [router current-session login]}]
  {:query         [{:root/router (comp/get-query TopRouter)}
                   {:root/current-session (comp/get-query Session)}
                   [::uism/asm-id ::TopRouter]
                   {:root/login (comp/get-query Login)}]
   :ident         (fn [] [:component/id :top-chrome])
   :initial-state {:root/router          {}
                   :root/login           {}
                   :root/current-session {}}}
  (let [current-tab (some-> (dr/current-route this this) first keyword)]
    (dom/div :.flex.flex-col.h-full
      (dom/div :.h-16.flex-none.bg-white.flex.flex-row.border-b.items-center
        {:className "border-dark-navy/20"}
        (dom/img :.h-10.px-8 {:src "/logo.jpg"})
        (for [[tab label] [[:main  "Setup"]
                           [:intro  "Intro"]
                           [:card-demo "Cards"]]
              :let [selected? (= tab current-tab)]]
          (dom/div :.text-lg.uppercase.self-end.group.cursor-pointer
            {:key tab
             :onClick (fn [] (dr/change-route this [(name tab)]))}
            (dom/div :.px-4.group-hover:font-semibold
              label)
            (dom/div :.rounded-t-md.border-t-4.mx-1
              {:className (if selected?
                            "border-peach"
                            "border-white group-hover:border-peach/40")})))

        (dom/div :.flex-1)
        ;; todo - finish styling login
        #_(dom/div :.mx-4
          (ui-login login)))

      (dom/div :.bg-cool-gray.flex-1.p-4.text-dark-navy.font-sans
        (ui-top-router router)))))

(def ui-top-chrome (comp/factory TopChrome))

(defsc Root [this {:root/keys [top-chrome]}]
  {:query         [{:root/top-chrome (comp/get-query TopChrome)}]
   :initial-state {:root/top-chrome {}}}
  (ui-top-chrome top-chrome))
